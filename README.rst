===========
 dversions
===========

dversions is a very stupid little command line wrapper around Debian version
string comparisions.  I wrote this because I can never remember what the
actual command is, and I wanted some more user friendly output.

Usage::

    $ ./dversions 9.1.2 8.0.2-4 8.0.2-3
    8.0.2-3 <= 8.0.2-4 <= 9.1.2

    $ ./dversions -r 9.1.2 8.0.2-4 8.0.2-3
    9.1.2 >= 8.0.2-4 >= 8.0.2-3


Project
=======

* Home page: https://gitlab.com/warsaw/dversions
* Bugs: https://gitlab.com/warsaw/dversions/issues


Author
======

Copyright (C) 2016 Barry Warsaw <barry@debian.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

https://www.debian.org/legal/licenses/mit
