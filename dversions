#!/usr/bin/python3

"""Compare Debian package version numbers."""


__version__ = '0.1'


import sys

from apt_pkg import init, version_compare
from argparse import ArgumentParser
from functools import cmp_to_key


LTE = ' <= '
GTE = ' >= '


def parseargs():
    parser = ArgumentParser(
        description="""Compare Debian version numbers.  In the default
                    operation, you can just provide some version numbers,
                    and this program will print a string displaying their
                    order from earliest to latest.   Various options control
                    the output and exit codes.""")
    parser.add_argument('--version',
                        action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('-r', '--reverse',
                        action='store_true', default=False,
                        help="""Reverse the sort order.""")
    parser.add_argument('strings', nargs='+')
    return parser.parse_args()


def main():
    init()
    args = parseargs()
    in_order = sorted(args.strings,
                      key=cmp_to_key(version_compare),
                      reverse=args.reverse)
    joiner = (GTE if args.reverse else LTE)
    print(joiner.join(in_order))
    return 0


if __name__ == '__main__':
    sys.exit(main())
